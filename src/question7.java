import java.util.Scanner;

public class question7 {

		public static void main(String[] args)
		{
			int number;
			char choice;
			int max = Integer.MIN_VALUE;
			int min = Integer.MAX_VALUE;
			
			Scanner s = new Scanner(System.in);
			do {
				
					System.out.println("Please enter the number");
					number = s.nextInt ();
					if(number>max) {
						max = number;
					}
					if(number<min)	{
						min = number;
					}
					System.out.println("Please enter y or n to continue");
					choice = s.next().charAt (0);
			}
			while(choice == 'y');
			System.out.println("largest is : " +max);
			System.out.println("smallest is : " +min);
		}
}

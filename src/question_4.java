import java.util.Scanner;

public class question_4 {
	public static void main (String[] args)
	{
		
		Scanner input  = new Scanner(System.in);
		
		
		int number;
		int reverse = 0;
		int reminder = 0;
		
		
		System.out.print(" Enter the number ");
		number  = input.nextInt();
		
		int temp = number;
		
			while(temp>0)
			{
			reminder = temp % 10;
			reverse = reverse * 10 + reminder;
				temp  /= 10;
			
			}
			
			System.out.println(" Reverse of "  + number + " is " + reverse);
	}

}